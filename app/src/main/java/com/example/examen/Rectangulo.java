package com.example.examen;

import java.io.Serializable;

public class Rectangulo implements Serializable {
    private int base;
    private int altura;

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public Rectangulo(){

    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public float CalcularPerimetro(){

        return (this.base * 2) + (this.altura * 2);
    }

    public float CalcularArea(){
        return this.base * this.altura;
    }
}
