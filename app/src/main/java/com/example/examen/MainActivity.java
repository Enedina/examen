package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnEntrar;
    private Button btnSalir;
    private EditText txtUsuario;
    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnTerminar);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);

        rectangulo = new Rectangulo();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = txtUsuario.getText().toString();
                if (usuario.matches("")) {
                    Toast.makeText(MainActivity.this, "Capturar todos los campos", Toast.LENGTH_LONG).show();
                }
                else{
                    Intent intent = new Intent (MainActivity.this,RectanguloActivity.class);
                    intent.putExtra("usuario","Usuario: "+usuario);

                    startActivity(intent);
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }
}






