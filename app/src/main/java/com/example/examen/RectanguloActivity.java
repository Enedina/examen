package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {
    private TextView lblNombre;
    private Rectangulo rectangulo;

    private EditText txtBase;
    private EditText txtAltura;

    private Button btnCalcular;
    private Button btnRegresar;
    private Button btnLimpiar;

    private TextView lblArea;
    private TextView lblPerimetro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);
        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);

        lblArea = (TextView) findViewById(R.id.lblArea);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);

        btnCalcular=(Button) findViewById(R.id.btnCalcular);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        lblNombre = (TextView) findViewById(R.id.lblNombre);
        Bundle datos = getIntent().getExtras();
        lblNombre.setText(datos.getString("usuario"));
        rectangulo = new Rectangulo();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtBase.getText().toString().matches("") || txtAltura.getText().toString().matches("")) {
                    Toast.makeText(RectanguloActivity.this, "Capturar todos los campos", Toast.LENGTH_LONG).show();
                } else {
                    int bas = Integer.valueOf(txtBase.getText().toString());
                    int alt = Integer.valueOf(txtAltura.getText().toString());
                    rectangulo.setBase(bas);
                    rectangulo.setAltura(alt);
                    lblPerimetro.setText(""+rectangulo.CalcularPerimetro());
                    lblArea.setText(""+rectangulo.CalcularArea());
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.getText().clear();
                txtBase.getText().clear();

            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
